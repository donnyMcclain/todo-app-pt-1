import React, { Component } from "react";
import todosList from "./todos.json";

class App extends Component {
  constructor(props){
    super(props);
    this.state = {todos: todosList};
  }
handleNewTodo = (e) => {
  if (e.keyCode === 13) {
    let newArray = this.state.todos;
    console.log (newArray);
    let idNum = newArray.length;
    idNum += 1;
    console.log(idNum);
    newArray.push({ userId: 1, id: idNum, title: e.target.value, completed: false});
    this.setState({ todos: newArray });
    return;
    }
}
handleCheck = (e) => {
 let index = e.currentTarget.id;
 let newArray = this.state.todos;
 if (newArray[index].completed === true) {
    newArray[index].completed = false;
  } else {
    newArray[index].completed = true;
  }
  this.setState({ todos: newArray });
  return;
};
handleDelOne = (e) => {
  let todoDel = e.currentTarget.id;
  let newArray = this.state.todos;
  newArray.splice(todoDel, 1);
  this.setState({ todos: newArray });
  return;
};

handelDelCompleted = (e) =>{
  let newArray = this.state.todos.filter(
    todos => todos.completed === false
  );
  this.setState({ todos: newArray });
  return;
};

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
          className="new-todo"
          placeholder="What needs to be done?"
          autoFocus
          onKeyDown={this.handleNewTodo}/>
        </header>
        <TodoList
        todos={this.state.todos}
        handleCheck={this.handleCheck}
        handleDelOne={this.handleDelOne}/>
        <footer className="footer">
          <span className="todo-count">
          <strong>{this.state.todos.length}</strong> item(s) left
          </span>
          <button
          className="clear-completed"
          onClick ={this.handelDelCompleted}>
              Clear completed
          </button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  handleCheck = (e) =>{
    this.props.handleCheck(e);
  }
  handleDelOne =(e) =>{
    this.props.handleDelOne(e);
  }
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input
          className="toggle"
          type="checkbox"
          id={this.props.id}
          checked={this.props.completed}
          onChange={this.handleCheck}/>
          <label>{this.props.title}</label>
          <button
          className="destroy"
          id ={this.props.id}
          onClick={this.handleDelOne}/>
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
handleCheck = (e) =>{
  this.props.handleCheck(e);
}
handleDelOne = (e) =>{
  this.props.handleDelOne(e);
}  

  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem
            key={todo.id}
            id={todo.id -1}
            title={todo.title}
            completed={todo.completed}
            handleCheck={this.handleCheck}
            handleDelOne={this.handleDelOne}/>
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
